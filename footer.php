			<footer>
				<div class="padding4040 col-md-12 footer">
					<div class="container">

						<div class="paddingbottom25 row">

							<!-- Upper footer -->
							<div class="col-md-3 col-sm-3">
								<?php the_field('footer_1','option'); ?>
							</div>
							<div class="col-md-3 col-sm-3">
								<?php the_field('footer_2','option'); ?>
							</div>
							<div class="col-md-3 col-sm-3">
								<?php the_field('footer_3','option'); ?>
							</div>
							<div class="col-md-3 col-sm-3">
								<?php the_field('footer_4','option'); ?>

								<!-- Social Icons -->
								<?php if(get_field('social_media_items','option')): ?>
									<ul class="social">
										<?php while(has_sub_field('social_media_items','option')): ?>
											<li><a href="<?php the_sub_field('icon_link'); ?>"><img src="<?php the_sub_field('image_icon'); ?>" alt=""></a></li>
										<?php endwhile; ?>
									</ul>
								<?php endif; ?>

								<div class="clearfix"></div>
							</div>
						</div>

						<!-- Bottom footer -->
						<div class="footer-bar">
							<div class="footer-inner-bar">
								<div class="footer-nav pull-left">
									<?php echo footer_nav(); ?>
								</div>
								<div class="pull-right">
									<span><?php the_field('copyright','option'); ?></span>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>


					</div>
				</div>
			</footer>

			</div> <!-- .container -->
		
		</div> <!-- end .outside-container .remielevangeliomark -->
				
		<!--[if lt IE 7 ]>
  			<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
  			<script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
		<![endif]-->
		
		<?php wp_footer(); // js scripts are inserted using this function ?>

		<script>
		jQuery(document).ready(function($) {
			$('.more-less-btn').click(function(e) {
				$('.more-content').stop(true).slideToggle();
				var moreText = $(this).find('.more-text').text();
				var cross = $(this).find('.cross').text();

				if(cross == "-") {
					$(this).find('.more-text').text('More');
					$(this).find('.cross').text('+')
				} else {
					$(this).find('.more-text').text('Less');
					$(this).find('.cross').text('-')
				}
				return false;
				e.preventDefault();
			});

			$(window).load(function(){
				$(window).resize(function() {
					$("#slider").carouFredSel({
						responsive: true,
						items: {
						width: $(window).outerWidth(),
						height: $(".caroufredsel_wrapper .slider-item").outerHeight()
						},
						scroll: {
							onAfter: function(){
							
								if($(window).outerWidth()>=1170) {
								//passive slider
								$(".caroufredsel_wrapper .slider-item img").css({opacity: "1"});
								$(".caroufredsel_wrapper .slider-item:first-child img, .caroufredsel_wrapper .slider-item:nth-child(3) img").animate({opacity: "0.4"},600);
								$(".caroufredsel_wrapper .slider-item:first-child .caption, .caroufredsel_wrapper .slider-item:nth-child(3) .caption").fadeOut();
								//active slider
								$(".caroufredsel_wrapper .slider-item:nth-child(2) img").animate({opacity: "1"},800);
								$(".caroufredsel_wrapper .slider-item:nth-child(2) .caption").fadeIn();
								} else {
								
								$(".caroufredsel_wrapper .slider-item:nth-child(1) img").animate({opacity: "1"},800);
								$(".caroufredsel_wrapper .slider-item:nth-child(1) .caption").fadeIn();
								}
							},		
							duration: 1000
						},
						next: "#sliderNext",
						prev: "#sliderPrev",
						auto: 8000
					
					});	
					if($(window).outerWidth()>=1170) {
						$(".caroufredsel_wrapper .slider-item:first-child img, .caroufredsel_wrapper .slider-item:nth-child(3) img").animate({opacity: "0.4"});
						$(".caroufredsel_wrapper .slider-item:nth-child(2) .caption").show();
					} else {
						$(".caroufredsel_wrapper").height($(".caroufredsel_wrapper .slider-item").outerHeight());
						$(".caroufredsel_wrapper .slider-item:nth-child(1) .caption").show();
					}
				});
				$(this).resize();
			});

			$('.main-menu-container').meanmenu({
				meanMenuContainer: '.mean-nav-here',
				meanScreenWidth: "750"
			});


		});
		</script>



	</body>

</html>