<?php
/*
Template Name: Homepage
*/
?>

<?php get_header(); ?>
	<!-- Slider -->
	<div class="slider">
		<div class="cst-slider container">
			<div class="row">
				<div id="slider">
					<?php if( get_field('cst_slider') ) : while( has_sub_field('cst_slider') ) : ?>
						<li class="slider-item">
							<div class="slider-item-wrap">
								<img src="<?php the_sub_field('slider_image'); ?>" />
								<div class="caption">
									<h4><?php the_sub_field('slider_caption_title'); ?></h4>
									<p><?php the_sub_field('slider_caption'); ?></p>
								</div>
							</div>
						</li>
					<?php endwhile; endif; ?>
				</div>
				<div class="slider-navigation">
					<span id="sliderPrev"><img src="<?php bloginfo('template_directory')?>/images/slider-nav-left-hover.png" /></span>
					<span id="sliderNext"><img src="<?php bloginfo('template_directory')?>/images/slider-nav-right-hover.png" /></span>
				</div>
		    </div>
		</div>
	</div>	

	<div class="inner-content">
		<div class="">

			<!-- Hot links -->
			<div class="hot-links">
				<div class="container">
					<div class="row">
						<div class="redbg col-md-4 col-sm-4">
							<div class="relative-div">
								<div class="hot-links-div row">
									<h3><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon1.png" /> <?php the_field('hotbox_1_title'); ?></h3>
									<span><?php the_field('hotbox_1_desc'); ?></span>
									<span class="small">*Zero investment Required</span>
									<a href="<?php the_field('hotbox_1_link_url'); ?>" class="special-btn"><?php the_field('hotbox_1_link_caption'); ?> >></a>
								</div>
							</div>
						</div>
						<div class="redbg col-md-4 col-sm-4">
							<div class="relative-div">
								<div class="hot-links-div row">
									<h3><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon2.png" /> <?php the_field('hotbox_2_title'); ?></h3>
									<span><?php the_field('hotbox_2_desc'); ?></span>
									<a href="<?php the_field('hotbox_2_link_url'); ?>" class="special-btn"><?php the_field('hotbox_2_link_caption'); ?> >></a>
								</div>
							</div>
						</div>
						<div class="redbg col-md-4 col-sm-4">
							<div class="relative-div">
								<div class="hot-links-div row">
									<h3><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon3.png" /> <?php the_field('hotbox_3_title'); ?></h3>
									<span><?php the_field('hotbox_3_desc'); ?></span>
									<a href="<?php the_field('hotbox_3_link_url'); ?>" class="special-btn"><?php the_field('hotbox_3_link_caption'); ?> >></a>
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>


			<!-- Intro  -->
			<div class="intro-div">
				<h2><?php the_field('yellow_box_title'); ?></h2>
				<span class="tag"><?php the_field('yellow_box_description'); ?></span>

				<!-- More / Less Button -->
				<a class="more-less-btn" href="#"><span class="more-text">Less</span> Info <span class="cross">-</span></a>
			</div>


			<!-- More Content -->
			<div class="more-content padding4040 bgwhite">
				<div class="row">
					<div class="container">
						<div class="col-md-6 col-sm-6">
							<div class="title">
								<h2><?php the_field('more_info_title_1'); ?></h2>
							</div>
							<div class="orange-separator"></div>

							<!-- Video -->
							<div class="video-wrap">
								<?php the_field('more_info_video_area'); ?>
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="title">
								<h2><?php the_field('more_info_title_2'); ?></h2>
							</div>
							<div class="orange-separator"></div>
							
							<span><?php the_field('more_info_tagline'); ?></span>
							<?php if(get_field('benefit_list')) { ?>

								<ul class="home-check-list">
									<?php while(has_sub_field('benefit_list')): ?>
										<li><span><?php the_sub_field('benefit'); ?></span></li>
									<?php endwhile;  ?>
								</ul>

							<?php } ?>
						</div>
					</div>
				</div>
			</div>



			<!-- Other intro -->
			<div class="other-intro bggray">
				<div class="container">
					<h2 class="pull-left"><?php the_field('grey_box_title'); ?></h2>
					<span class="text pull-left"><?php the_field('grey_box_description'); ?></span>
					<div class="clearfix"></div>
				</div>
			</div>


		</div>
	</div>
<?php get_footer(); ?>